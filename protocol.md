## Introduction

Here you can find the detail of the specifications for the protocol, along with the installation and configuration guidelines.

## Bluetooth configuration (Network)

For the current version we are using [PyBluez](https://github.com/pybluez/pybluez) and Raspberry Bluetooth as base layers to connect devices. 

#### Install dependencies

```
sudo apt install python3-bluez libbluetooth-dev
```

#### Running bluetooth in compatibility mode

Open `/etc/systemd/system/dbus-org.bluez.service`, and modify the following line 

`ExecStart=/usr/[...]/bluetooth/bluetoothd` 

with 

`ExecStart=/usr/[...]/bluetooth/bluetoothd -C`

This will add a file socket for the bluetooth interface for sdp advertisement, that any other program than the bluetooth module could manipulate.

#### Setting no timeout for discovery

There is a default timeout (3 mins) when the RPi is in discoverable mode. We need to disable the timeout to allow constant discovery.

Open `/etc/bluetooth/main.conf` and uncomment/add the line `DiscoverableTimeout = 0`

#### Reloading bluetooth services

Once done we need to reload bluetooth services
```
sudo systemctl daemon-reload
sudo systemctl restart bluetooth.service
```

#### Executing server as non-root

Follow [this post](https://github.com/ev3dev/ev3dev/issues/274#issuecomment-74593671) in order to be able to run RFCOMM server without root permission.

Basically, it changes the file permission on the `/var/run/sdp` socket created by the compatibility mode.

## RFCOMM connection (Transport)

Device creating a RFCOMM server with a name beginning with "connect4-", then a user custom name.

Timeout for packets during a game: 1 minute

If connection is lost, delete grid and go back on start menu.

## Codetable (Application)

Code | Meaning | Action.s upon reception | Type
--- | --- | --- | ---
000 - 006 | Column number | Put token into right column | `byte`
100 | Asking for a game (asker will play in human mode) | Respond with 102 or 103 | `byte`
101 | Asking for a game (asker will play in AI mode) | Respond with 102 or 103 | `byte`
102 | Game accepted | Launch grid, wait for askee to play | `byte`
103 | Game refused | - | `byte`
201 | Game aborted by user (during a game) _For now, we wait our turn to send this? (beware of this)_ | Abandon game (this way we know the game was ended because of user request, not because of disconnection) | `byte`

## Misc

