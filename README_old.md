**REMIND THIS IS NOT A FIXED, DEFINITIVE VERSION** (WIP)

Guidelines for the network multiplayer mode in connect4 from the inter-device communication committee (IDCC).

For the current version we are using [PyBluez](https://github.com/pybluez/pybluez) and Raspberry Bluetooth as base layers to connect devices. 

## Installation
### Install dependencies
```
sudo apt-get install libbluetooth-dev
sudo apt-get install python3-bluez
```
### Edit bluetooth configuration file
There is a default timeout (3 mins) when the RPi is in discoverable mode. We need to disable the timeout to allow constant discovery.

Open `/etc/bluetooth/main.conf` and uncomment/add the line `DiscoverableTimeout = 0`

Once done we need to reload bluetooth services
```
sudo systemctl daemon-reload
sudo systemctl restart bluetooth.service
```
Last step would be to make our device discoverable by entering in the terminal
```
bluetoothctl disverable on
```
Examples of usage are under `examples/`

---